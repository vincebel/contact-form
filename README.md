# README #

A contact form I made using HTML/CSS and PHP.

Includes statements for inserting each form submitted into a MySQL database (commented out)

### ABOUT ###

* This is a contact form that emails the form contents to a specified email address, and can store the form contents in a MySQL database.
* Version 1.0.0

### SETUP ###

* Put the 3 files (contact.php, style.css, and space.jpg) in your web hosting's root directory.
* Change the $email variable on contact.php line 29 to the email you wish to receive the emails at.
* Make sure your server is configured to allow outgoing mail.

### MYSQL STORAGE ###

* To enable MySQL storage, uncomment lines 21-27 on contact.php, and change the $host, $username, and $password variables to your database's credentials. 
* Note that the table's name in the SQL statement is contactForms, change this to whatever your table's name is.
* You must also have columns in your table named 'firstname', 'lastname', 'email', 'phone', 'message', and 'date' of the proper data types.