<!--HEADER-->
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/css?family=Space+Mono" rel="stylesheet">

<!--BODY-->
<div class="header">
    CONTACT US
</div>
<?php
if(isset($_POST['submit'])){
    //Get form information and stores to variables
    $firstname=$_POST['firstname'];
    $lastname=$_POST['lastname'];
    $email=$_POST['email'];
    $phone=$_POST['phone'];
    $message=nl2br($_POST['message']);
    
    $name = $firstname . " " . $lastname;
    
    //Insert contact form into a MySQL database if needed.
        //$host="localhost";
        //$username="username";
        //$password="password";
        //$db = new PDO($host, $username, $password);
        
        //$statement="INSERT INTO contactForms(firstname, lastname, email, phone, message, date) VALUES('$firstname', '$lastname', '$email', '$phone', '$message', NOW())";
        //$query=$db->query($statement);
    
    $to="test@gmail.com";
    $subject="New message from " . $name;
    $message="You have received the following message:"
    . "\nFrom: " . $name
    . "\nEmail: " . $email
    . "\nPhone: " . $phone
    . "\n\n" . $message;
    $headers="From:" . $email;
    
    if(mail($to,$subject,$message,$headers)){
       ?>
       <div class="contactform">
       Thank you for contacting us <?php echo $firstname; ?>, we will get back to you shortly.
       <h4><a id="return" href="contact.php">Return</a></h4>
       </div>
    <?php }
}
else{
?>
<div class="contactform">
    <form method="post">
        <div class="row">
            <div class="col">
                <label>First Name</label>
                <input type="text" name="firstname" placeholder="Anakin" required>
            </div>
            <div class="col">
                <label>Last Name</label>
                <input type="text" name="lastname" placeholder="Skywalker" required>
            </div>
        </div>
        <div class="row">
            <label>Email address</label>
            <input type="email" name="email" placeholder="podracer17@tatooine.com" required>
            
            <label>Phone Number</label>
            <input type="number" name="phone" placeholder="(555)-555-5555">
            
            <label>Message</label>
            <textarea rows="6" cols="50" name="message" placeholder="Your message here..."></textarea>
            
            <button id="submitbutton" type="submit" name="submit">Transmit Message</button>
        </div>
    </form>
</div>
<?php }